const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available with our frontend application
    // Cross Origin Resource Sharing
const cors = require("cors");
const app = express();
// This syntax will allow flexibility when using the application locally or as a hosted app
const port = process.env.PORT || 4000;
const userRoutes = require("./routes/userRoutes");

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.tovusyc.mongodb.net/b203_to_do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Allow all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" strnig to be included for all user routes in the "userRoutes" file
app.use("/users", userRoutes);



app.listen(port, () => console.log(`API is now online on port ${port}`));