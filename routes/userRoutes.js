const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")

console.log(userControllers);

router.post("/checkEmail", userControllers.checkEmailExists);
router.get("/allUsers", userControllers.getAllUsers);
router.post("/register", userControllers.registerUser);
router.post("/login", userControllers.loginUser);
router.get("/details/:userId", userControllers.getProfile);



module.exports = router;


