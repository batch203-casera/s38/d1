const jwt = require("jsonwebtoken");

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret key
const secret = "CourseBookingAPI";

// [SECTION] JSON Web Tokens
// Token Creation
/* 
    Analogy:
        Pack the gift provided with a provided lock, which can only be open using the secret code as the key
*/
// The "user" parameter will contain the value of the user upon login
module.exports.createAccessToken = (user) => {
    console.log(user);
    // payload of the JWT
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    // Generate a JSON web token using the jwt's "sign method"
    /* 
        Syntax:
            jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])
    */
   return jwt.sign(data, secret, {})
}



