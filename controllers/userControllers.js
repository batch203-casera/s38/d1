// import the User model in the controllers instead because this is wehre we are now going to use it.
const User = require("../models/User");

// import bcrypt
const bcrypt = require("bcrypt");
// bcrypt is a package which allows us to hash our passwords to add a layer of security for our users' details

// import auth.js module to use createAccessToken and its subsequent methods
const auth = require("../auth");


/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) => {
    return (User.find({email: req.body.email})).then(result => {
        // The result of the find() method returns an array of objects
        // We can use array.length method for checking the current result length
        console.log(result);

        // The user already exists
        if(result.length > 0){
            return res.send(true);
        } else {
            return res.send(false); // There are no duplicates found
        }
    })
    .catch(error => res.send(error))
}



module.exports.getAllUsers = (req, res) => {
    User.find({})
    .then(tasks => {res.send(tasks)})
    .catch(error => res.send(error));
} 


// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {

    let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        // Syntax: bcrypt.hashSync(dataToBeEncrypted, salt);
            // salt - salt rounds that the bcrypt algrithm will round to encrypt the password
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNumber: req.body.mobileNumber
    });
    console.log(newUser);

    return newUser.save()
    .then(user => {
        console.log(user);
        res.send(true);
    })
    .catch(error => {
        console.log(error);
        res.send(false);
    })
}

// User authentication (login)
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (req, res) => {
    return User.findOne({email: req.body.email})
    .then(result => {
        // User does not exist
        if(result == null){
            return res.send({message: "No User Found!"});
        } else {
            // User exists
            // Syntax: compareSync(data, encrypted)
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
            // If the passwords match/result of the above code is true
            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(result)})
            } else {
                return res.send({message: "Incorrect password!"});
            }
        }
    })
}

// S38 Activity
/*
    1. Create a "/details/:id" route that will accept the user’s Id to retrieve the details of a user.
    2. Create a getProfile controller method for retrieving the details of the user:
        - Find the document in the database using the user's ID
        - Reassign the password of the returned document to an empty string ("") / ("*****")
        - Return the result back to the postman
    3. Process a GET request at the /details/:id route using postman to retrieve the details of the user.
    4. Updated your s37-41 remote link, push to git with the commit message of "Add activity code - S38".
    5. Add the link in Boodle.

*/

module.exports.getProfile = (req, res) => {
    let updates = {
        password: req.body.password
    };

    User.findByIdAndUpdate(req.params.userId, updates, {new: true})
    .then(updateUser => res.send(updateUser))
    .catch(error => res.send(error));
}


